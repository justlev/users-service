const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

exports.authenticate = (email, password, callback) => {
    User.findOne({ email: email })
    .exec(function (err, user) {
      if (err) {
        return callback(err)
      } else if (!user) {
        var err = new Error('User not found.');
        err.status = 401;
        return callback(err);
      }
      bcrypt.compare(password, user.password, function (err, result) {
        if (result === true) {
          return callback(null, user);
        } else {
          return callback();
        }
      })
    });
}

exports.valid_password = (password, compare_to, success, failure) =>{
    bcrypt.compare(password, compare_to, function (err, result) {
        if (result === true) {
          return success();
        } else {
          return failure();
        }
      });
};

exports.valid_password_sync = (password, compare_to) =>{
    return bcrypt.compareSync(password, compare_to);
};

exports.generateToken = (user_id) => {
    return jwt.sign({
        id: user_id,
      }, process.env.COOKIE_SESSSION_SECRET, {
        expiresIn: 120
      });
};