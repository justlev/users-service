const facebookClient = require('../../clients/facebook');

exports.loginOrCreateUser = function(accessToken, onSuccess, onFail){
    facebookClient.validateFbToken(accessToken, (facebookObj) => {
        if (facebookObj.id){
            user_id = facebookObj.id;
            User.findOne({external_id: user_id, user_source: 'facebook'}, (err, user) => {
                if (err){
                    onFail(err);
                }
                else if (user==null || user.length == 0){
                    createUserFromFbObject(facebookObj, (user) => onSuccess(user, true), onFail);
                }
                else{
                    onSuccess(user, false);
                }
            });
        }
        else{
            onFail("facebookObject wan't correct");
        }
    });
}

exports.registerUser = function(accessToken, onSuccess, onFail){
    facebookClient.getFbUserObject(accessToken, (fbUser) => {
        createUserFromFbObject(fbUser, onSuccess, onFail);
    }, error => {
        onFail(error);
    });
}

function createUserFromFbObject(fbUser, onSuccess, onFail){
    const user = new User(parseUser(fbUser));
    user.save((err, savedUser) => {
        if (err){
            onFail(err);
        }
        else{
            onSuccess(savedUser);
        }
    });
}

function parseUser(fbUser){
    return {
        first_name: fbUser.first_name,
        last_name: fbUser.last_name,
        user_source: "facebook",
        external_id: fbUser.id,
        date_of_birth: fbUser.birthday,
        email: fbUser.email
    };
}