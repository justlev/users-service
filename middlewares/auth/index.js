const public_apis = require('./public_apis');
const expressJwt = require('express-jwt');  
const authenticate = expressJwt({secret : process.env.COOKIE_SESSSION_SECRET});

function validate_auth(req, res, next) {
    if (public_apis[req.path] !== undefined) {
      return next();
    }
    authenticate(req, res, next);
 }

module.exports = validate_auth;