const passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy;

const password_validator = require('../services/auth');

passport.use(new LocalStrategy(
    {
        session: false,
        usernameField: 'email',
        passwordField: 'password'
    },

    function(username, password, done) {
      User.findOne({ email: username }, function (err, user) {
        if (err) { return done(err); }
        if (!user) {
          return done(null, false, { message: 'Incorrect username.' });
        }
        if (!password_validator.valid_password_sync(password, user.password)) {
          return done(null, false, { message: 'Incorrect password.' });
        }
        return done(null, user);
      });
    }
  ));

  module.exports = passport;