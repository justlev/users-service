require('dotenv').config();
require('./models');
require('./config/passport');
const bodyParser = require("body-parser");
const auth = require('./middlewares/auth');
const passport = require('passport');
const filter = require("./middlewares/errors_filter");

const express = require('express');
const app = express();

var root_router = require('./api');

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use(auth);
app.use(passport.initialize());
app.use(passport.session());

app.use('/', root_router);
app.use(filter);

app.listen(8080, function(){
    console.log('listening on 8080');
});
