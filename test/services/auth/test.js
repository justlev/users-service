const chai = require('chai');
const expect = chai.expect
const simple = require('simple-mock')
const mock = require('mock-require')

const mock_bcrypt = {compareSync: function(){
  return true;
}};
mock('bcrypt', mock_bcrypt);

const subject = require("../../../services/auth");

describe('valid_password_sync', function() {
  describe('correct_password_provided', function() {
    it('should return true', function() {
      mock_bcrypt.compareSync = () => true;
        expect(subject.valid_password_sync("test", "test")).to.equal(true);
    });
  });

  describe('incorrect_password_provided', function() {
    it('should return false', function() {
       mock_bcrypt.compareSync= () => false;
        expect(subject.valid_password_sync("test", "test")).to.equal(false);
    });
  });
});