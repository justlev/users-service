const request = require('request');

exports.validateFbToken = function(token, onResponse, onError){
    performRequest("get", userObjectUrl(token), onResponse, onError);
}

exports.getFbUserObject = function(token, onResponse, onError){
    performRequest("get", userObjectUrl(token), onResponse, onError);
}

function performRequest(method, url, onSuccess, onError){
    request(url, { method: method, json: true }, (err, res, body) => {
        if (err) {
             onError(err); 
             return;
        }
        onSuccess(body)
    });
}

function userObjectUrl(accessToken){
    return `https://graph.facebook.com/me?fields=id,first_name,last_name,birthday,email&access_token=${accessToken}`;
}