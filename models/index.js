const mongoose = require('mongoose');
const Schema = mongoose.Schema

const user = require('./user')
const phone = require('./phone')
const address = require('./address')

mongoose.connect(process.env.DB_CONNECTION, {useNewUrlParser:true}, (err) => {
    if (err){
        console.log("Failed to connect! "+err);
    } 
    else{
        console.log("Connected successfully to DB.");
    }
}
);

const Phone = mongoose.model(phone.name, phone.data)
const Address = mongoose.model(address.name, address.data)
const User = mongoose.model(user.name, user.data)

global.User = User;
global.Phone = Phone;
global.Address = Address;