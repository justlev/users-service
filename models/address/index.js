const mongoose = require('mongoose');
const Schema = mongoose.Schema

module.exports.name = 'address'
module.exports.data = new Schema({
    country: String,
    city: String,
    street: String
}, {timestamps: true})