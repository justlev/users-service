const mongoose = require('mongoose');
const Schema = mongoose.Schema

module.exports.name = 'phone'
module.exports.data = new Schema({
    country_code: Number,
    phone: String,
    validated: { type:Boolean, default: false }
}, {timestamps: true});