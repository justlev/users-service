const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const Schema = mongoose.Schema

const phone_model = require('../phone');
const address_model = require('../address');

module.exports.name = 'user'
 const schema = new Schema({
    first_name: {type: String, required: true},
    last_name: {type: String, required: true},
    email: {type: String, required: true, unique : true},
    date_of_birth: {type: Date, required: true},
    phone: phone_model.data,
    address: address_model.data,
    password: {type: String },
    external_id: { type: String },
    user_source: { type: String }
}, {timestamps: true});

schema.pre('save', function (next) {
    const user = this;
    if (!user.password){
      next();
    }
    bcrypt.hash(user.password, 10, function (err, hash){
      if (err) {
        return next(err);
      }
      user.password = hash;
      next();
    })
  });
module.exports.data = schema;