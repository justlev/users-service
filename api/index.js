var express = require('express');
var router = express.Router();

var registration_api = require('./v1/registration_api');
var login_api = require('./v1/login_api');
var info_api = require('./v1/info');

router.post('/register', registration_api.register);
router.post('/login', login_api.login);
router.get('/info', info_api.info);

module.exports = router