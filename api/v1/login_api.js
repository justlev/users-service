const passport = require('passport');
const auth_service = require('../../services/auth');
const facebookService = require('../../services/facebook');

exports.login = (req, res, next) => {
    if (req.body.type == "facebook") {
        facebookAuth(req.body.token, res);
    }
    else{
        localAuth(req, res, next);
    }
};

function localAuth(req, res, next){
    passport.authenticate("local", (error, user, info) => {
        if (error){
            sendResponseError(res, "Something went wrong", 500);
            return;
        }
        if (!user){
            sendResponseError(res, info, 401);
            return;
        }
        sendResponseTokenAndUser(res, user, false);
    })(req,res,next);
}

function facebookAuth(accessToken, res){
    facebookService.loginOrCreateUser(accessToken, (user, newUser) => {
        sendResponseTokenAndUser(res, user, newUser);

    }, (error) => {
        sendResponseError(res, error, 401);
    });
}

function sendResponseTokenAndUser(res, user, newUser){
    const userDetails = {first_name: user.first_name, last_name: user.last_name, date_of_birth: user.date_of_birth, email: user.email};
    const toReturn = {...userDetails, userId: user.id, newUser: newUser, token: auth_service.generateToken(user.id)};
    res.send(toReturn);
}

function sendResponseError(res, err, statusCode){
    res.statusCode = statusCode;
    res.send(err);
}