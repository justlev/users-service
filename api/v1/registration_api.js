const auth_service = require('../../services/auth');
const facebookService = require('../../services/facebook');

exports.register =  (req, res) => {
    body = req.body;
    if (body.type == "facebook"){
        facebookService.registerUser(req.body.accessToken, (user) => {
            responseSendToken(res, user);
        }, error => {
            res.statusCode = 400;
            res.send(error);
        });
    }
    else{
        createUser(body, res);
    }
}

function createUser(body, res){
    user = new User(body);
    user.save(((err, user) => {
        if (err){
            res.send(err)
        }
        else{
            responseSendToken(res, user);
        }
    }));
}

function responseSendToken(res, user){
    res.send({
        token: auth_service.generateToken(user.id)
    });
}