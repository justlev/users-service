FROM node:8
WORKDIR /usr/src/app
COPY package*.json ./
RUN apt-get update && apt-get install -y build-essential && apt-get install -y python && npm install
RUN npm install bcrypt
RUN npm install --only=production
COPY . .
EXPOSE 8080
CMD ["node", "server.js"]